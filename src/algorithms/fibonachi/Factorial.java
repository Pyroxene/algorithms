package algorithms.fibonachi;

public class Factorial {


    //fact 5 = 5*4*3*2*1=120
    public int findFactorial(int number) {
        if (number == 0) {
            return 1;
        }
        if (number == 1) {
            return 1;
        }

        int result = number;
        for (int i = number; i > 1; i--) {
            result = result * (i - 1);
        }

        return result;
    }

    public int findFactorialRecursevly(int number) {
        if (number > 1) {
            return findFactorialRecursevly(number - 1) * number;
        }else{return 1;}
    }

    public static void main(String[] args) {
        Factorial factorial = new Factorial();
        System.out.println(factorial.findFactorial(7));
        System.out.println(factorial.findFactorialRecursevly(7));
    }

}
