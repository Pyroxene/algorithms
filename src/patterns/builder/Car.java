package patterns.builder;

public class Car {
    private int numOfWheels;
    private int numOfDoors;
    private String color;
    private String plateNumber;

    private Car() {}


    public static CarBuilder newCarBuilder(){
        return new Car().new CarBuilder();
    }

    public class CarBuilder{

        private Car carStub;

        public CarBuilder(){
            carStub = new Car();
        }

        public CarBuilder numOfWheels(int numOfWheels){
            carStub.setNumOfWheels(numOfWheels);
            return this;
        }
        public CarBuilder numOfDoors(int numOfDoors){
            carStub.setNumOfDoors(numOfDoors);
            return this;
        }
        public CarBuilder color(String color){
            carStub.setColor(color);
            return this;
        }
        public CarBuilder plateNum(String plateNum){
            carStub.setPlateNumber(plateNum);
            return this;
        }

        public Car build(){
            return carStub;
        }
    }


    //Setters

    public void setNumOfWheels(int numOfWheels) {
        this.numOfWheels = numOfWheels;
    }

    public void setNumOfDoors(int numOfDoors) {
        this.numOfDoors = numOfDoors;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "numOfWheels=" + numOfWheels +
                ", numOfDoors=" + numOfDoors +
                ", color='" + color + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                '}';
    }

    public static void main(String[] args) {
        CarBuilder builder = newCarBuilder();
        Car someCar = builder.color("Green").numOfDoors(7).numOfWheels(8).plateNum("YYY66qwe").build();
        System.out.println("Here is some car");
        System.out.println(someCar);
    }
}
