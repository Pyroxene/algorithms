package algorithms.sorting;

import java.util.Arrays;

public class MergeSort {
//Получаем массив, передаем в перегруженный метод, включающий в себя создание временного с такой же длиной
//+ левыйСтарт = 0, правыйКонец = индексу последднего элемента
    public void mergeSort(int[] array) {
        mergeSort(array, new int[array.length], 0, array.length - 1);
    }

    private void mergeSort(int[] array, int[] temp, int leftStart, int rightEnd) {
//если 0 >= последнему индексу. В случае когда в массиве всего 1 элемент. Конец.
        if (leftStart >= rightEnd) {
            return;
        }
// середина = левыйСтарт + правый конец / 2
//вызываем метод с коноцом равным середине
//вызываем метод с началом равным середине
//Будут дробится пока не получится много единичных массивов
//сливаем массивы
        int middle = (leftStart + rightEnd) / 2;
        mergeSort(array, temp, leftStart, middle);
        mergeSort(array, temp, middle + 1, rightEnd);
        mergeHalves(array, temp, leftStart, rightEnd);
    }

    private void mergeHalves(int[] array, int[] temp, int leftStart, int rightEnd) {
        int leftEnd = (rightEnd + leftStart) / 2;
        int rightStart = leftEnd + 1;
        int size = rightEnd - leftStart + 1;

        int left = leftStart;
        int right = rightStart;
        int index = leftStart;

        while (left <= leftEnd && right <= rightEnd) {
            if(array[left] <= array[right]){
                temp[index] = array[left];
                left++;
            }else {
                temp[index] = array[right];
                right++;
            }
            index++;
        }

        System.arraycopy(array, left, temp, index, leftEnd - left +1);
        System.arraycopy(array, right, temp, index, rightEnd - right +1);
        System.arraycopy(temp, leftStart, array, leftStart, size);
    }

    public static void main(String[] args) {
        int [] array = new int[2];
        array[0] = 17;
        array[1] = 4;
//        array[2] = 3;
//        array[3] = 1;
//        array[4] = 10;
//        array[5] = 2;

        MergeSort mergeSort = new MergeSort();
        System.out.println("Before: " + Arrays.toString(array));
        mergeSort.mergeSort(array);
        System.out.println("After: " + Arrays.toString(array));
    }

}
