package patterns;

public class Singleton {

    private Singleton(){}
    private static Singleton instanceOfSingleton;

    public static Singleton getInstance(){
        if (instanceOfSingleton == null){
            instanceOfSingleton = new Singleton();
        }
        return instanceOfSingleton;
    }

}
