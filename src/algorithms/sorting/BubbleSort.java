package algorithms.sorting;

import java.util.Arrays;

public class BubbleSort {

    public void bubbleSort(int[] array) {
        int temp;
        boolean isSorted = false;
        int diminishingLength = array.length - 1;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < diminishingLength; i++) {
                if (array[i] > array[i + 1]) {
                    temp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = temp;
                    isSorted = false;

                }
            }
            diminishingLength--;
        }
    }

    public static void main(String[] args) {
        BubbleSort bubbleSort = new BubbleSort();

        int[] array = new int[10];
        array[0] = 1;
        array[1] = 3;
        array[2] = 5;
        array[3] = 4;
        array[4] = 7;
        array[5] = 10;
        array[6] = 12;
        array[7] = 8;
        array[8] = 9;
        array[9] = 4;

        System.out.println("Before sort: " + Arrays.toString(array));
        bubbleSort.bubbleSort(array);
        System.out.println("After sort: " + Arrays.toString(array));
    }

}
